# Mariadb backup with restic

This container performs a backup of a mysql/mariadb instance by:

- Performing a `mysqldump` of all databases with restic
- Sending the output to remote storage (currently only S3-compatible supported)

## Environment variables

| Variable name         | Description                                                                                                                                      |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| S3_ENDPOINT           | Location of your S3 storage. Defaults to "https://vault.ecloud.co.uk"                                                                            |
| AWS_ACCESS_KEY_ID     | Key id for s3 storage. Required                                                                                                                  |
| AWS_SECRET_ACCESS_KEY | Key secret for s3 storage. Required                                                                                                              |
| MARIADB_HOST          | Your database host. Required                                                                                                                     |
| MARIADB_USER          | Defaults to "root"                                                                                                                               |
| MARIADB_PORT          | Defaults to 3306                                                                                                                                 |
| MARIADB_PWD           | Password to connect to database. One of MARIADB_PWD or MARIADB_PWD_FILE must be provided                                                         |
| MARIADB_PWD_FILE      | File containing the password to connect to database. One of MARIADB_PWD or MARIADB_PWD_FILE must be provided                                     |
| BUCKET_NAME           | S3 bucket for your Restic repository. The bucket must exist, but if a Restic repository doesn't, this container will create it for you. Required |
| FILENAME              | Filename/path restic will use for your snapshots. Defaults to 'mariadb'                                                                          |
| ENABLE_PRUNE          | Whether to forget and prune old snapshots. Value is either 0 or 1. Default is `1`                                                                |
| KEEP_LAST             | How many snapshots to keep. Defaults to 100                                                                                                      |
| RESTIC_PASSWORD       | Restic password env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                         |
| RESTIC_PASSWORD_FILE  | Restic password file env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                    |

## Example

```
docker run --rm \
  -e BUCKET_NAME=mariadb \
  -e AWS_ACCESS_KEY_ID=<my-key-id> \
  -e AWS_SECRET_ACCESS_KEY=<my-key-secret> \
  -e RESTIC_PASSWORD=<my-password> \
  -e MYSQL_PWD=<mariadb-password> \
  -e MARIADB_HOST=mariadb \
  j3asolutions/mariadb-restic
```
