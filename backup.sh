#!/bin/bash

set -euo pipefail

export RESTIC_REPOSITORY=s3:$S3_ENDPOINT/$BUCKET_NAME
PASSWORD=

if [ -z "${MARIADB_PWD_FILE+x}" ]
then
  PASSWORD=$MARIADB_PWD
else
  PASSWORD=$(cat $MARIADB_PWD_FILE)
fi

echo "Starting backup with $(restic version)"

echo "Checking snapshots and whether repository exists";
restic snapshots --cache-dir $RESTIC_DIR && REPO_EXISTS=$? || REPO_EXISTS=$?

if [ ! $REPO_EXISTS -eq 0 ]
then
  echo "Creating repository in $RESTIC_REPOSITORY if it does not exist"
  restic init;
fi

mysql --host=$MARIADB_HOST --port=$MARIADB_PORT --user=$MARIADB_USER --password=$PASSWORD &>/dev/null && CONNECTION_OK=$? || CONNECTION_OK=$?

if [ ! $CONNECTION_OK -eq 0 ]
then
  echo "Unable to connect to database"
  exit 1;
fi

BACKUP_DB_ARG=

if [ -z ${MARIADB_DATABASES+x} ]; then 
  BACKUP_DB_ARG="--all-databases"
else
  BACKUP_DB_ARG="--databases ${MARIADB_DATABASES}"
fi

mariadb-dump \
  --compress \
  --host=$MARIADB_HOST \
  --port=$MARIADB_PORT \
  --user=$MARIADB_USER \
  --password=$PASSWORD \
  $BACKUP_DB_ARG \
  | restic backup --cache-dir $RESTIC_DIR --stdin --verbose --stdin-filename $FILENAME

if [ $ENABLE_PRUNE -eq 1 ]
then
  restic forget --prune --group-by paths --cache-dir $RESTIC_DIR --keep-last $KEEP_LAST;
fi