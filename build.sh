#!/bin/sh

set -e

export DOCKER_BUILDKIT=1

DOCKER_REGISTRY=docker.io/j3asolutions/restic-mariadb
TAG=$1
LATEST_TAG=$DOCKER_REGISTRY:latest;

echo "Building image $LATEST_TAG";

docker build --tag "$LATEST_TAG" .;

docker push $LATEST_TAG;

if [ -n "$TAG" ]; then
  VERSIONED_TAG=$DOCKER_REGISTRY:$TAG;

  echo "Tagging image $VERSIONED_TAG";

  docker tag $LATEST_TAG $VERSIONED_TAG;
  docker push $VERSIONED_TAG;
fi